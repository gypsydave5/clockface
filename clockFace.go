package clockface

import (
	"math"
	"time"
)

// Hands returns the angle in radians from 12 o'clock of the hour, minute and
// second hands of a clock face.
func Hands(t time.Time) (hour, minute, second float64) {
	hour = hoursInRadians(t)
	minute = minutesInRadians(t)
	second = secondsInRadians(t)
	return
}

func HandVectors(t time.Time) (hour, minute, second Vector) {
	return Vector{0, 0}, Vector{0, 0}, Vector{0, 0}
}

type Vector struct {
	x float64
	y float64
}

func circle(radius, angle float64) (x float64, y float64, err error) {
	x = roundFloat(radius*math.Sin(angle), 10)
	y = roundFloat(radius*math.Cos(angle), 10)
	return
}

func minutesInRadians(t time.Time) (radians float64) {
	var secondsInHour float64 = 60 * 60
	minutesAndSeconds := float64(t.Minute()*60 + t.Second())
	secondAsRadian := (2 * math.Pi) / secondsInHour
	return secondAsRadian * minutesAndSeconds
}

func secondsInRadians(t time.Time) (radians float64) {
	var secondsInMinute float64 = 60
	seconds := float64(t.Second())
	secondAsRadian := (2 * math.Pi) / secondsInMinute
	return secondAsRadian * seconds
}

func hoursInRadians(t time.Time) (radians float64) {
	var secondsIn12Hours float64 = 60 * 60 * 12
	minutesAndSeconds := float64(t.Minute()*60 + t.Second())
	hours := float64((t.Hour() % 12) * 60 * 60)
	secondAsRadian := (2 * math.Pi) / secondsIn12Hours
	return secondAsRadian * (hours + minutesAndSeconds)
}

func roundFloat(x float64, precision int) float64 {
	p := float64(precision * 10)
	return math.Round(x*p) / p
}
