package clockface

import (
	"fmt"
	"io"
	"time"
)

// WriteSVGClockface takes a Time and an io.Writer and writes an SVG
// clockface representation of the time to the io.Writer
func WriteSVGClockface(t time.Time, out io.Writer) (err error) {
	hr := hoursInRadians(t)
	hx, hy, _ := circle(40, hr)

	mr := minutesInRadians(t)
	mx, my, _ := circle(70, mr)

	sr := secondsInRadians(t)
	sx, sy, _ := circle(90, sr)

	out.Write([]byte(svgHeader()))
	out.Write([]byte(svgOpenTag()))
	out.Write([]byte(bezel()))
	out.Write([]byte(hourHand(hx, hy)))
	out.Write([]byte(minuteHand(mx, my)))
	out.Write([]byte(secondHand(sx, sy)))
	out.Write([]byte(svgCloseTag()))
	return
}

func svgHeader() string {
	return `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">`
}

func svgOpenTag() string {
	return `<svg xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     xmlns:serif="http://www.serif.com/"
     width="100%"
     height="100%"
     viewBox="0 0 300 300"
     version="1.1"
     xml:space="preserve"
     style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">`
}

func svgCloseTag() string {
	return "</svg>"
}

func hourHand(x, y float64) string {
	return fmt.Sprintf("<line x1=\"150\" y1=\"150\" x2=\"%f\" y2=\"%f\""+
		" style=\"fill:none;stroke:#000;stroke-width:7px;\"/>", x+150, 150-y)
}

func minuteHand(x, y float64) string {
	return fmt.Sprintf("<line x1=\"150\" y1=\"150\" x2=\"%f\" y2=\"%f\""+
		" style=\"fill:none;stroke:#000;stroke-width:7px;\"/>", x+150, 150-y)
}

func secondHand(x, y float64) string {
	return fmt.Sprintf("<line x1=\"150\" y1=\"150\" x2=\"%f\" y2=\"%f\""+
		" style=\"fill:none;stroke:#f00;stroke-width:3px;\"/>", x+150, 150-y)
}

func bezel() string {
	return `<circle cx="150" cy="150" r="100" style="fill:#cccffa;stroke:#000;stroke-width:5px;"/>
  <path d="M150,71.794c0,-8.767 0,-20.187 0,-20.187" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M150,249.614c0,-8.767 0,-20.188 0,-20.188" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M105.545,73.612c-2.192,-3.796 -5.047,-8.741 -5.047,-8.741" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M199.502,236.35c-2.192,-3.796 -5.047,-8.742 -5.047,-8.742" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M73.002,106.155c-3.796,-2.191 -8.741,-5.046 -8.741,-5.046" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M235.739,200.112c-3.796,-2.192 -8.741,-5.047 -8.741,-5.047" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M71.184,150.61c-8.767,0 -20.187,0 -20.187,0" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M249.003,150.61c-8.766,0 -20.187,0 -20.187,0" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M73.002,195.065c-3.796,2.192 -8.741,5.047 -8.741,5.047" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M235.739,101.109c-3.796,2.191 -8.741,5.046 -8.741,5.046" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M105.084,228.407c-1.991,3.449 -4.586,7.943 -4.586,7.943" style="fill:none;stroke:#000;stroke-width:5px;"/>
  <path d="M199.502,64.871c-2.192,3.796 -5.047,8.741 -5.047,8.741" style="fill:none;stroke:#000;stroke-width:5px;"/>`
}
