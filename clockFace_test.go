package clockface

import (
	"math"
	"testing"
	"time"
)

func TestCircleCalculation(t *testing.T) {
	var testData = []struct {
		name   string
		radius float64
		angle  float64
		xCoord float64
		yCoord float64
	}{
		{"pointing up", 50, 0.0, 0, 50},
		{"pointing down", 50, math.Pi, 0.0, -50},
		{"pointing right", 50, math.Pi / 2, 50, 0},
		{"pointing left", 50, 3 * (math.Pi / 2), -50, 0},
	}

	for _, data := range testData {
		t.Run(data.name, func(t *testing.T) {
			x, y, _ := circle(data.radius, data.angle)
			if x != data.xCoord {
				t.Errorf("Expected x of %v, but got %v", data.xCoord, x)
			}
			if y != data.yCoord {
				t.Errorf("Expected y of %v, but got %v", data.yCoord, y)
			}
		})
	}
}

func TestHandVectors(t *testing.T) {
	var testData = []struct {
		name   string
		time   time.Time
		hour   Vector
		minute Vector
		second Vector
	}{
		{"00:00:00", newClockTime(0, 0, 0), Vector{0, 0}, Vector{0, 0}, Vector{0, 0}},
	}

	for _, d := range testData {
		t.Run(d.name, func(t *testing.T) {
			hour, minute, second := HandVectors(d.time)
			if hour != d.hour {
				t.Errorf("expected hour vector of %v but got %v", d.hour, hour)
			}
			if minute != d.minute {
				t.Errorf("expected minute vector of %v but got %v", d.minute, minute)
			}
			if second != d.second {
				t.Errorf("expected second vector of %v but got %v", d.second, second)
			}
		})
	}
}

func TestHands(t *testing.T) {
	var testData = []struct {
		name   string
		time   time.Time
		hour   float64
		minute float64
		second float64
	}{
		{"00:00:00", newClockTime(0, 0, 0), 0, 0, 0},
		{"03:00:00", newClockTime(3, 0, 0), math.Pi / 2, 0, 0},
		{"15:00:00", newClockTime(15, 0, 0), math.Pi / 2, 0, 0},
		{"23:00:00", newClockTime(23, 0, 0), 11 * (math.Pi / 6), 0, 0},
		{"00:30:00", newClockTime(00, 30, 0), math.Pi / 12, math.Pi, 0},
		{"00:45:00", newClockTime(00, 45, 0), 3 * (math.Pi / 24), 3 * (math.Pi / 2), 0},
		{"06:15:00", newClockTime(06, 15, 0), 25 * (math.Pi / 24), math.Pi / 2, 0},
		{
			"01:01:30",
			newClockTime(01, 01, 30),
			(60*60 + 60 + 30) * (math.Pi / (60 * 60 * 6)),
			(60 + 30) * (math.Pi / (60 * 30)),
			math.Pi,
		},
		{
			"13:01:30",
			newClockTime(01, 01, 30),
			(60*60 + 60 + 30) * (math.Pi / (60 * 60 * 6)),
			(60 + 30) * (math.Pi / (60 * 30)),
			math.Pi,
		},
	}

	for _, d := range testData {
		t.Run(d.name, func(t *testing.T) {
			hour, minute, second := Hands(d.time)
			if !closeEnough(hour, d.hour) {
				t.Errorf("expected hour angle  of %v but got %v", d.hour, hour)
			}
			if !closeEnough(minute, d.minute) {
				t.Errorf("expected minute angle  of %v but got %v", d.minute, minute)
			}
			if !closeEnough(second, d.second) {
				t.Errorf("expected second angle  of %v but got %v", d.second, second)
			}
		})
	}
}

func newClockTime(hour, minute, second int) time.Time {
	return time.Date(0, time.January, 0, hour, minute, second, 0, time.UTC)
}

func closeEnough(a, b float64) bool {
	float64EqualityThreshold := 1e-7
	return math.Abs(a-b) < float64EqualityThreshold
}
