package main

import (
	"gitlab.com/gypsydave5/clockface"
	"os"
	"time"
)

func main() {
	clockface.WriteSVGClockface(time.Now(), os.Stdout)
	os.Exit(0)
}
